package main

import(
    "sort"
    "net/http"
    "encoding/json"
    "net/url"
    "fmt"
    "io/ioutil"
    "bufio"
    "strings"
    "os"
    "gopkg.in/yaml.v2"
)

// STRUCT FOR BOOKNAME OUTPUT
type Book  struct {
    Docs []struct {
	Title string `json:"title"`
	AuthorName []string `json:"author_name"`
	AuthorKey []string `json:"author_key"`
	} `json:"docs"`
} 

// STRUCT FOR AUTHOR WORKS OUTPUT
type Works struct {
	Docs          []struct {
		Key         string   `json:"key"`
		Type        string   `json:"type"`
		Name        string   `json:"name"`
		Works       string   `json:"top_work"`
		WorkCount   int      `json:"work_count"`
		TopSubjects []string `json:"top_subjects"`
		Version     int64    `json:"_version_"`
	} `json:"docs"`
}

func main() {
    // Read input from CLI
    fmt.Print("Enter name of book: ")
    reader := bufio.NewReader(os.Stdin)
    // ReadString will block until the delimiter is entered
    title, err := reader.ReadString('\n')
    if err != nil {
	fmt.Println("An error occured while reading input. Please try again", err)
	return
    }

    // Remove the delimeter from the string
    title = strings.TrimSuffix(title, "\n") 

    // Modify input so it can be safely placed inside a URL query
    newTitle := url.QueryEscape(title)

    // Set proper URL based on input
    urlFirst := fmt.Sprintf("https://openlibrary.org/search.json?title=%s", newTitle)

    // Send request to API
    req, err := http.NewRequest("GET", urlFirst, nil )
    if err != nil {
        fmt.Print(err.Error())
    }

    res, err := http.DefaultClient.Do(req)
    if err != nil {
        fmt.Print(err.Error())
    }

    // Close connection to API
    defer res.Body.Close()
    body, readErr := ioutil.ReadAll(res.Body)
    if readErr != nil {
        fmt.Print(err.Error())
    }

    // Parse data from API to struct in JSON
    var convertedBook Book
    json.Unmarshal(body, &convertedBook)

    // Create request to Authors API based on name of an author from user's input

    // Concatenate the strings in the AuthorName array into a single string
    author := strings.Join(convertedBook.Docs[0].AuthorName, " ")

    // Modify input so it can be safely placed inside a URL query
    newAuthor := url.QueryEscape(author)
    
    // Set proper URL based on input
    authorUrl := fmt.Sprintf("https://openlibrary.org/search/authors.json?q=%s", newAuthor)

    // Send request to API
    authorReq, err := http.NewRequest("GET", authorUrl, nil )
    if err != nil {
        fmt.Print(err.Error())
    }

    authorRes, err := http.DefaultClient.Do(authorReq)
    if err != nil {
        fmt.Print(err.Error())
    }

    // Close connection to API
    defer authorRes.Body.Close()
    authorBody, readErr := ioutil.ReadAll(authorRes.Body)
    if readErr != nil {
        fmt.Print(err.Error())
    }

    // Parse data from API to struct in JSON
    var convertedWorks Works
    json.Unmarshal(authorBody, &convertedWorks)

    // Print author's works from the authors query and parse output from JSON to YAML
    name := convertedWorks.Docs[0].Name
    //works, err := yaml.Marshal(convertedWorks.Docs[0].TopSubjects)
    works := convertedWorks.Docs[0].TopSubjects

    // Read input from the CLI
    fmt.Print("Enter 'asc' to sort the output in ascending order, or 'desc' to sort it in descending order: ")
    workReader := bufio.NewReader(os.Stdin)
    // ReadString will block until the delimiter is entered
    order, err := workReader.ReadString('\n')
    if err != nil {
        fmt.Println("An error occured while reading input. Please try again", err)
        return
    }

    // Remove the delimeter from the string
    order = strings.TrimSuffix(order, "\n")

    // Sort the slice in ascending or descending order based on user input
    if order == "asc" {
	// Sort the slice in ascending order
	sort.Slice(works, func(i int, j int) bool { return works[i] < works[j] })
    	newWorks, _ := yaml.Marshal(works)
	// Print output
	fmt.Printf("Author's name: %s\n", name)
	fmt.Printf("Author's works: \n%s", newWorks)
    } else if order == "desc" {
	// Sort the slice in descending order
	sort.Slice(works, func(i int, j int) bool { return works[j] < works[i] })
        newWorks, _ := yaml.Marshal(works)
	// Print output
	fmt.Printf("Author's name: %s\n", name)
	fmt.Printf("Author's works: \n%s", newWorks)
    } else {
	// Print error message
        fmt.Println("Invalid input. Please enter 'asc' or 'desc'")
        return
    }
} 
