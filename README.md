# Find author's works from API

Application for searching author's works from OpenLibrary API. Application will retrieve works from author of specific book from OpenLibrary API based on input from a user and sort the ouput in ascending or descending order also based on user's output.

## Installation

Use the git clone function to download source code.

```bash
git clone git@gitlab.com:lvasko/go-assignement.git
```

## Usage

```go
# run from CLI
go run main.go

# App will prompt you to enter a book name
Enter name of book: 

# App will then promt you to choose desired output (desc or asc)
Enter 'asc' to sort the output in ascending order, or 'desc' to sort it in descending order:

# At the end you will receive works from author of the book
Enter name of book: Practical Golang
Enter 'asc' to sort the output in ascending order, or 'desc' to sort it in descending order: asc
Author's name: Amit Saha
Author's works: 
- Communities
- Computer programming
- Data processing
- Mathematics
- Mathematics, data processing
- Mathematics, study and teaching
- Online social networks
- Python (Computer program language)
- Python (computer program language)
- Study and teaching
```
